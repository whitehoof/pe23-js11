"use strict"
const icons = document.querySelectorAll(".icon-password");
const form = document.querySelector(".password-form");
const inputs = form.querySelectorAll("input");
const MSG_INVALID = "Потрібно ввести однакові значення";
const MSG_VALID = "You are welcome";

// створюю елемент, який відображатиме ситуацію з неоднаковими паролями:
const spanInvalid = document.createElement("span");
spanInvalid.innerText = MSG_INVALID;
spanInvalid.style.color = "#f30";
spanInvalid.style.display = "none";
inputs[1].insertAdjacentElement("afterend", spanInvalid);


// вішаю на іконки слухач, який переключатиме видимість пароля:
for (let icon of icons) {
	icon.addEventListener("click", e => {
		// спершу, переключаю іконку:
		e.target.classList.toggle("fa-eye");
		e.target.classList.toggle("fa-eye-slash");
		// тепер на основі класів іконки, переключаю тип того інпута, який знаходиться всередині того ж батька, що і клікнута іконка: 
		if (e.target.classList.contains("fa-eye")) {
			e.target.parentElement.querySelector("input").type = "password";
		} else {
			e.target.parentElement.querySelector("input").type = "text";
		};
	});
};

// вішаю submit-слухач на форму:

form.addEventListener("submit", e => {
	// відміняю перезавантаження сторінки:
	e.preventDefault();
	// порівнюю значення в інпутах
	if (inputs[0].value === inputs[1].value) {
		// прибираю повідомлення про різні паролі (якщо було виведене) та виводжу алерт про успіх:
		spanInvalid.style.display = "none";
		alert("You are welcome");
	} else {
		// виводжу повідомлення про різні паролі:
		spanInvalid.style.display = "inline";
	};
});
